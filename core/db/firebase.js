var admin = require("firebase-admin");
const config = require("../../config");
const emoji = require("node-emoji");

module.exports = () => {
  admin.initializeApp({
    credential: admin.credential.cert(config.firebase),
    databaseURL: config.firebase.databaseURL,
  });
  console.log(`Firebase initialized: ${config.firebase.project_id}`, emoji.get("white_check_mark"));
};