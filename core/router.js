var express = require("express");
var fs = require("fs");
const emoji = require("node-emoji");

const {loggerTabbed} = require("./helpers");
const { DEEPLEVEL_INCREMENT } = require("./consts");

const registerFolder = (path, deepLevel = 0) => {
  loggerTabbed(`${emoji.get("open_file_folder")} ${path.replace(process.cwd(), "").replace("\\", "/")}`, deepLevel);

  let router = express.Router();
  let files = fs.readdirSync(path);
  
  if (path.includes("bypass_")) {
    loggerTabbed(`${emoji.get("fire")} Router Core Bypass: ${path.replace(process.cwd(), "").replace("\\", "/")}/index.js`, deepLevel + DEEPLEVEL_INCREMENT);
    router.use(require(`${path}/index.js`));
    return router;
  }

  //register middlewares
  if (fs.existsSync(`${path}/middlewares.js`) && fs.lstatSync(`${path}/middlewares.js`).isFile()) {
    Object.values(require(`${path}/middlewares.js`)).forEach((middleware) => {
      router.use(middleware);
      loggerTabbed(emoji.get("arrow_right") + "Middleware: " + middleware.name, deepLevel + DEEPLEVEL_INCREMENT);
    });
  }

  files.forEach((file) => {
    if (["middlewares.js"].includes(file)) return;
    if (file.startsWith("_")) return;//skip files with underscore to avoid load of helpers
  
    if (fs.lstatSync(path + "/" + file).isDirectory()) {
      router.use(registerFolder(path + "/" + file, deepLevel + DEEPLEVEL_INCREMENT));
    } else {
      if (file.startsWith("bypass_")) {
        loggerTabbed(emoji.get("fire") + " Router Bypass: " + path.replace(process.cwd(), "").replace("\\", "/") + "/" + file, deepLevel + DEEPLEVEL_INCREMENT);
        router.use(require(`${path}/${file}`));
      } else if (file.includes("index.js")) {
        const route = require(path + "/" + file);
        router.use(require("./autoload")[route.strategy ?? "default"](route, `${path}`, deepLevel + (DEEPLEVEL_INCREMENT)));
      } else {
        const route = require(path + "/" + file);
        router.use(require("./autoload")[route.strategy ?? "default"](route, `${path}/${file}`, deepLevel + (DEEPLEVEL_INCREMENT)));
      }
    }
  });

  return router;
}

module.exports = registerFolder;