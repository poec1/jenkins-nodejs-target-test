module.exports = {
  cors: ["https://admin.socket.io", "http://127.0.0.1:5173"],
  
  admin: {
    auth: false,
    mode: "development",

    // If you want to enable authentication:
    //username: "",
    //password: "", //bcrypt hash

    // if you want to use read-only mode:
    readonly: true,
  }
}