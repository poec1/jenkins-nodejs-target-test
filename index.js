require('module-alias/register');

const fs = require('fs');
const express = require('express');
const config = require('./config');
const colors = require('colors');
const app = express();
const server = require('http').createServer(app);

global.app = app;
global.server = server;

/* INTIALIZATION */
require("@core/env");
require("@core/helpers").launcherScreen();
//require("@core/db/firebase")();
//require("@core/db/prisma")(); //disabled for step 1 refonte
console.log("");//separator
/* INTIALIZATION */

/* EXTERNAL CORE PLUGINS */
app.use(require("cors")({origin: config.http.cors}));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.all("/healthcheck", (req, res) => {res.set("Cache-Control", "no-store, no-cache, must-revalidate, private"); res.send("OK");});
app.use(require('express-status-monitor')());
/* EXTERNAL CORE PLUGINS */

/* BuildNumber route. Optionnal for startup*/
try {
  const BuildNumber = fs.readFileSync(__dirname + "/buildnumber.txt", "utf8");
  if (BuildNumber) {
    console.log("BuildNumber: " + BuildNumber);
    app.get("/buildnumber", (req, res) => {
      res.set("Cache-Control", "no-store, no-cache, must-revalidate, private"); 
      return res.send(BuildNumber);
    });
  }
} catch (e) {console.log("BuildNumber not found");}
/* BuildNumber route */

/* INTERNAL CORE PLUGINS */
require("@core/services");
console.log("");//separator
app.use(require('@core/router')(__dirname + "/controllers"));
//require("@core/socketio"); //disabled for step 1 refonte
/* INTERNAL CORE PLUGINS */

server.listen(config.http.port, () => {
  //require("./core/helpers").showEmojiDataset("arrow");
  console.log("");
  console.log(colors.green(`--- Server listening on port ${config.http.port} ---`));
  console.log("");
});

process.on('SIGTERM', () => {
  console.log('SIGTERM signal received: closing HTTP server')
  server.close(() => {
    console.log('Goodbye!')
  })
})
